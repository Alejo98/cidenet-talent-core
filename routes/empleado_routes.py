# app.py
import sqlite3
from flask import Flask, request, Blueprint, jsonify, render_template
from flask_cors import cross_origin

empleados_bp = Blueprint('empleados_bp', __name__)

app = Flask(__name__)

def get_db_connection():
    conn = sqlite3.connect('empleados.db')
    conn.row_factory = sqlite3.Row
    return conn

def ejecutar_query(query, params=None, commit=False):
    connection = get_db_connection()
    cur = connection.cursor()

    try:
        if params:
            cur.execute(query, params)
        else:
            cur.execute(query)

        if commit:
            connection.commit()
        else:
            return cur.fetchall()
    except Exception as e:
        connection.rollback()
        raise e
    finally:
        connection.close()

@app.route('/registrar', methods=['POST'])
@cross_origin(origin='http://localhost:4200', headers=['Content-Type', 'Authorization'])
def registrar_empleado():
    datos_nuevos = request.json

    query = "INSERT INTO empleado (primer_apellido, segundo_apellido, primer_nombre, otros_nombres, pais_empleo, tipo_identificacion, numero_identificacion, correo_electronico, fecha_ingreso, area, estado, fecha_registro) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
    params = (datos_nuevos['primer_apellido'], datos_nuevos['segundo_apellido'], datos_nuevos['primer_nombre'], datos_nuevos['otros_nombres'], datos_nuevos['pais_empleo'], datos_nuevos['tipo_identificacion'], datos_nuevos['numero_identificacion'], datos_nuevos['correo_electronico'], datos_nuevos['fecha_ingreso'], datos_nuevos['area'], datos_nuevos['estado'], datos_nuevos['fecha_registro'])

    try:
        ejecutar_query(query, params, commit=True)
        return jsonify({'mensaje': 'Empleado registrado exitosamente'}), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 500

@app.route('/api/empleados')
@cross_origin(origin='http://localhost:4200', headers=['Content-Type', 'Authorization'])
def consultar_empleados():
    filtro_nombre = request.args.get('filtroNombre', '')

    if filtro_nombre:
        query = "SELECT * FROM empleado WHERE primer_nombre LIKE ?"
        params = (f'%{filtro_nombre}%',)
    else:
        query = "SELECT * FROM empleado"
        params = None

    empleados = ejecutar_query(query, params)

    return jsonify([dict(empleado) for empleado in empleados])

@app.route('/<int:empleado_id>', methods=['DELETE'])
@cross_origin(origin='http://localhost:4200', headers=['Content-Type', 'Authorization'])
def eliminar_empleado(empleado_id):
    connection = get_db_connection()
    cur = connection.cursor()

    cur.execute("DELETE FROM empleado WHERE id=?", (empleado_id,))

    if cur.rowcount > 0:
        connection.commit()
        return jsonify({'mensaje': 'Empleado eliminado exitosamente'}), 200
    else:
        connection.rollback()
        return jsonify({'error': 'Empleado no encontrado'}), 404

@app.route('/<int:empleado_id>', methods=['GET'])
@cross_origin(origin='http://localhost:4200', headers=['Content-Type', 'Authorization'])
def obtener_detalle_empleado(empleado_id):
    connection = get_db_connection()
    cur = connection.cursor()

    cur.execute("SELECT * FROM empleado WHERE id=?", (empleado_id,))
    empleado = cur.fetchone()

    connection.close()

    if empleado:
        return jsonify(dict(empleado))
    else:
        return jsonify({'error': 'Empleado no encontrado'}), 404

@app.route('/<int:empleado_id>', methods=['PUT'])
@cross_origin(origin='http://localhost:4200', headers=['Content-Type', 'Authorization'])
def editar_empleado(empleado_id):
    connection = get_db_connection()
    cur = connection.cursor()

    cur.execute("SELECT * FROM empleado WHERE id=?", (empleado_id,))
    empleado = cur.fetchone()

    if empleado:
        datos_nuevos = request.json

        for key, value in datos_nuevos.items():
            cur.execute(f"UPDATE empleado SET {key}=? WHERE id=?", (value, empleado_id))

        try:
            connection.commit()
            return jsonify({'mensaje': 'Empleado editado exitosamente'}), 200
        except Exception as e:
            connection.rollback()
            return jsonify({'error': str(e)}), 500
        finally:
            connection.close()
    else:
        return jsonify({'error': 'Empleado no encontrado'}), 404

if __name__ == '__main__':
    try:
        app.run(debug=True)
    except Exception as e:
        print(f"Error al ejecutar la aplicación: {str(e)}")
    finally:
        print("Cerrando la aplicación...")
