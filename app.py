from flask import Flask, request, Blueprint, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin
from flask_migrate import Migrate
import sqlite3

app = Flask(__name__)

# Configuración de CORS de manera global
CORS(app, supports_credentials=True)

# Configuración de la base de datos
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///empleados.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Inicialización de SQLAlchemy y Flask-Migrate
db = SQLAlchemy(app)
migrate = Migrate(app, db)


# Función para obtener una conexión a la base de datos SQLite
def get_db_connection():
    conn = sqlite3.connect('empleados.db')
    conn.row_factory = sqlite3.Row
    return conn


# Función genérica para ejecutar consultas SQL y manejar transacciones
def ejecutar_query(query, params=None, commit=False):
    connection = get_db_connection()
    cur = connection.cursor()

    try:
        if params:
            cur.execute(query, params)
        else:
            cur.execute(query)

        if commit:
            connection.commit()
        else:
            return cur.fetchall()
    except Exception as e:
        connection.rollback()
        raise e
    finally:
        connection.close()


# Ruta para consultar empleados con filtro opcional por nombre
@app.route('/api/empleados/')
@cross_origin(origin='http://localhost:4200', headers=['Content-Type', 'Authorization'])
def consultar_empleados():
    filtro_nombre = request.args.get('filtroNombre', '')

    if filtro_nombre:
        query = "SELECT * FROM empleado WHERE primer_nombre LIKE ? OR otros_nombres LIKE ? OR primer_apellido LIKE ?"
        params = (f'%{filtro_nombre}%', f'%{filtro_nombre}%', f'%{filtro_nombre}%')
    else:
        query = "SELECT * FROM empleado"
        params = None

    empleados = ejecutar_query(query, params)
    return jsonify([dict(empleado) for empleado in empleados])


# Ruta para registrar un nuevo empleado
@app.route('/api/empleados/registrar', methods=['POST'])
@cross_origin(origin='http://localhost:4200', headers=['Content-Type', 'Authorization'])
def registrar_empleado():
    datos_nuevos = request.json

    query = "INSERT INTO empleado (primer_apellido, segundo_apellido, primer_nombre, otros_nombres, pais_empleo, tipo_identificacion, numero_identificacion, correo_electronico, fecha_ingreso, area, estado, fecha_registro) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
    params = (datos_nuevos['primer_apellido'], datos_nuevos['segundo_apellido'], datos_nuevos['primer_nombre'], datos_nuevos['otros_nombres'], datos_nuevos['pais_empleo'], datos_nuevos['tipo_identificacion'], datos_nuevos['numero_identificacion'], datos_nuevos['correo_electronico'], datos_nuevos['fecha_ingreso'], datos_nuevos['area'], datos_nuevos['estado'], datos_nuevos['fecha_registro'])

    try:
        print(query, params);
        ejecutar_query(query, params, commit=True)
        return jsonify({'mensaje': 'Empleado registrado exitosamente'}), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 500


# Ruta para eliminar un empleado por su ID
@app.route('/api/empleados/<int:empleado_id>', methods=['DELETE'])
@cross_origin(origin='http://localhost:4200', headers=['Content-Type', 'Authorization'])
def eliminar_empleado(empleado_id):
    query = "DELETE FROM empleado WHERE id=?"
    params = (empleado_id,)
    try:
        ejecutar_query(query, params, commit=True)
        return jsonify({'mensaje': 'Empleado eliminado exitosamente'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 404


# Ruta para obtener detalles de un empleado por su ID
@app.route('/api/empleados/<int:empleado_id>', methods=['GET'])
@cross_origin(origin='http://localhost:4200', headers=['Content-Type', 'Authorization'])
def obtener_detalle_empleado(empleado_id):
    query = "SELECT * FROM empleado WHERE id=?"
    params = (empleado_id,)
    empleado = ejecutar_query(query, params)
    
    if empleado:
        return jsonify(dict(empleado[0]))
    else:
        return jsonify({'error': 'Empleado no encontrado'}), 404


# Ruta para editar un empleado por su ID
@app.route('/api/empleados/<int:empleado_id>', methods=['PUT'])
@cross_origin(origin='http://localhost:4200', headers=['Content-Type', 'Authorization'])
def editar_empleado(empleado_id):
    query_select = "SELECT * FROM empleado WHERE id=?"
    params_select = (empleado_id,)
    empleado = ejecutar_query(query_select, params_select)

    if empleado:
        datos_nuevos = request.json

        # Actualizar cada campo modificado
        for key, value in datos_nuevos.items():
            query_update = f"UPDATE empleado SET {key}=? WHERE id=?"
            params_update = (value, empleado_id)
            print(query_update, params_update);
            ejecutar_query(query_update, params_update, commit=True)

        return jsonify({'mensaje': 'Empleado editado exitosamente'}), 200
    else:
        return jsonify({'error': 'Empleado no encontrado'}), 404


# Registrar las rutas después de la creación de la aplicación y configuración CORS
if __name__ == '__main__':
    from routes.empleado_routes import empleados_bp

    # Configuración CORS para Flask-Migrate
    CORS(empleados_bp)

    app.register_blueprint(empleados_bp)
    app.run(debug=True)
