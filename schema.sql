-- schema.sql

-- ... (otras definiciones de tablas si las tienes)

-- Definición de la tabla Empleado
CREATE TABLE empleado (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    primer_apellido TEXT NOT NULL,
    segundo_apellido TEXT NOT NULL,
    primer_nombre TEXT NOT NULL,
    otros_nombres TEXT,
    pais_empleo TEXT NOT NULL,
    tipo_identificacion TEXT NOT NULL,
    numero_identificacion TEXT UNIQUE NOT NULL,
    correo_electronico TEXT UNIQUE NOT NULL,
    fecha_ingreso DATE NOT NULL,
    area TEXT NOT NULL,
    estado TEXT DEFAULT 'Activo' NOT NULL,
    fecha_registro DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- ... (otras definiciones de tablas si las tienes)
