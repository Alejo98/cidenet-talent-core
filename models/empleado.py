from app import db

class Empleado(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    primer_apellido = db.Column(db.String(20), nullable=False)
    segundo_apellido = db.Column(db.String(20), nullable=False)
    primer_nombre = db.Column(db.String(20), nullable=False)
    otros_nombres = db.Column(db.String(50))
    pais_empleo = db.Column(db.String(20), nullable=False)
    tipo_identificacion = db.Column(db.String(50), nullable=False)
    numero_identificacion = db.Column(db.String(20), unique=True, nullable=False)
    correo_electronico = db.Column(db.String(300), unique=True, nullable=False)
    fecha_ingreso = db.Column(db.Date, nullable=False)
    area = db.Column(db.String(50), nullable=False)
    estado = db.Column(db.String(20), default="Activo", nullable=False)
    fecha_registro = db.Column(db.DateTime, default=db.func.current_timestamp(), nullable=False)

    def as_dict(self):
        return {column.name: getattr(self, column.name) for column in self.__table__.columns}
