# init_db.py
import sqlite3

# Conectar a la base de datos
connection = sqlite3.connect('empleados.db')

# Ejecutar el script de creación de tablas
with open('schema.sql') as f:
    connection.executescript(f.read())

# Crear un cursor
cur = connection.cursor()

# Insertar registros de prueba para la tabla Empleado
cur.execute("INSERT INTO empleado (primer_apellido, segundo_apellido, primer_nombre, otros_nombres, pais_empleo, tipo_identificacion, numero_identificacion, correo_electronico, fecha_ingreso, area, estado, fecha_registro) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            ('Apellido1', 'Apellido2', 'Nombre1', 'OtrosNombres', 'Pais1', 'Tipo1', '12345', 'correo1@example.com', '2024-01-23', 'Área1', 'Activo', '2024-01-23 00:00:00')
            )

cur.execute("INSERT INTO empleado (primer_apellido, segundo_apellido, primer_nombre, otros_nombres, pais_empleo, tipo_identificacion, numero_identificacion, correo_electronico, fecha_ingreso, area, estado, fecha_registro) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            ('Apellido3', 'Apellido4', 'Nombre2', 'OtrosNombres2', 'Pais2', 'Tipo2', '67890', 'correo2@example.com', '2024-01-24', 'Área2', 'Activo', '2024-01-24 00:00:00')
            )

# Commit de los cambios y cerrar la conexión
connection.commit()
connection.close()
