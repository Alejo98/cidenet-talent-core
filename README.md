# Cidenet Talent - Backend

Este proyecto corresponde al backend de la aplicación de registro de empleados de Cidenet S.A.S., desarrollado con Python y Flask.

## Instrucciones de Instalación y Ejecución

### Clonar el Repositorio

```bash
git clone https://gitlab.com/Alejo98/cidenet-talent-core.git cidenet-talent-core
cd cidenet-talent-core
```

### Crear y Activar el Entorno Virtual

```bash
python -m venv venv
source venv/bin/activate   # En sistemas Unix/Linux
venv\Scripts\activate      # En sistemas Windows
```

### Instalar Dependencias

```bash
pip install -r requirements.txt
```

### Configurar Variables de Entorno
Crear un archivo .env en la raíz del proyecto y configurar las variables de entorno necesarias.

### Ejecutar la Aplicación

python app.py

La aplicación estará disponible en http://localhost:5000/

